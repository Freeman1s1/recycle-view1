package com.example.itschool.commentlist;

import android.content.Context;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static com.example.itschool.commentlist.MainActivity.pos;

/**
 * Created by IT SCHOOL on 21.12.2016.
 */
public class Comment implements Comparable{
    private String username;
    private String img;
    private String comment;
    private int likes;
    private int views;
    private Date date;
    public Comment (String username, String comment, int likes, int views, String dat) {
        this.username = username;
        this.comment = comment;
        this.likes=likes;
        this.views=views;
        SimpleDateFormat format=new SimpleDateFormat();
        format.applyPattern("dd.MM.yyyy");
        try {
            this.date=format.parse(dat);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public int compareTo(Object obj) {
        if (obj instanceof Comment) {
            Comment com = (Comment) obj;
            if (pos == 0) {
                if (this.likes > com.likes) {
                    return -1;
                } else if (this.likes < com.likes) {
                    return 1;
                }

            }
            if (pos == 1){
                if (this.views > com.views) {
                    return -1;
                }else if(this.views < com.views){
                    return 1;
                }
            }
            if (pos == 2){
                if (this.date.after(com.date)) {
                    return -1;
                }else if(this.date.before(com.date)){
                    return 1;
                }
            }

        }
        return 0;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public Date getDate() {
        return date;
    }
}
