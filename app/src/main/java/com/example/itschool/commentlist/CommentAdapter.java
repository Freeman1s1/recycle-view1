package com.example.itschool.commentlist;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.zip.Inflater;

/**
 * Created by IT SCHOOL on 21.12.2016.
 */
public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.CommentViewHolder> {

    ArrayList<Comment> comments;
    Context context;

    CommentAdapter(Context context, ArrayList<Comment> list) {
        this.context = context;
        this.comments = list;
    }

    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        Log.d("MYLOG", "create view holder");
        View itemLayoutView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_layout, null);
        CommentViewHolder commentView = new CommentViewHolder(itemLayoutView);
        return commentView;
    }

    @Override
    public void onBindViewHolder(CommentViewHolder viewHolder, int i) {
        Log.d("MYLOG", "bind view holder");
        Comment comment = comments.get(i);
        viewHolder.mTVUsername.setText(comment.getUsername());
        viewHolder.mTVComment.setText(comment.getComment());
        viewHolder.mTVlikes.setText(String.valueOf(comment.getLikes()));
        viewHolder.mTVviews.setText(String.valueOf(comment.getViews()));
        viewHolder.mTVdate.setText(String.valueOf(comment.getDate()));
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }


    class CommentViewHolder extends RecyclerView.ViewHolder {
        ImageView mIVAvatar;
        TextView mTVUsername, mTVComment, mTVlikes, mTVviews, mTVdate;

        public CommentViewHolder(View rootView) {
            super(rootView);
            mIVAvatar = (ImageView) rootView.findViewById(R.id.iv_avatar);
            mTVComment = (TextView) rootView.findViewById(R.id.tv_comment);
            mTVUsername = (TextView) rootView.findViewById(R.id.tv_username);
            mTVlikes = (TextView) rootView.findViewById(R.id.tv_likes);
            mTVviews = (TextView) rootView.findViewById(R.id.tv_views);
            mTVdate = (TextView) rootView.findViewById(R.id.tv_date);
        }
    }


}
