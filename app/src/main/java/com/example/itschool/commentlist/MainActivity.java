package com.example.itschool.commentlist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    public static int pos;
    RecyclerView recyclerView;
    CommentAdapter adapter;
    Spinner spinner;
    private String[] variants = {"best", "popular", "newest"};
    ArrayList<Comment> list = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.recycle_view);
        spinner = (Spinner) findViewById(R.id.spinner);

        ArrayAdapter<String> spinneAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,
                variants);

        spinner.setAdapter(spinneAdapter);



        // ArrayList<Comment> list = new ArrayList<>();
        list.add(new Comment("user", "comment1", 56, 32, "02.03.2015"));
        list.add(new Comment("admin", "comment2", 562, 132, "02.07.2015"));
        list.add(new Comment("user", "comment1", 526, 312, "12.03.2005"));
        list.add(new Comment("admin", "comment2", 126, 323, "02.11.2015"));
        list.add(new Comment("user", "comment1", 346, 332, "22.03.2015"));
        list.add(new Comment("admin", "comment2", 21, 312, "02.03.2015"));
        list.add(new Comment("user", "comment1", 56, 32, "12.03.2017"));
        list.add(new Comment("admin", "comment2", 21, 3212, "03.03.2007"));
        list.add(new Comment("user", "comment1", 516, 32, "03.03.2015"));
        list.add(new Comment("admin", "comment2", 56, 32, "04.03.2015"));
        list.add(new Comment("user", "comment1", 56, 3212, "05.03.2015"));
        list.add(new Comment("admin", "comment2", 536, 32, "05.03.2015"));

        list.add(new Comment("user", "comment1", 516, 32, "31.03.1999"));
        list.add(new Comment("admin", "comment2", 456, 32, "02.03.2007"));
        list.add(new Comment("user", "comment1", 56, 332, "02.08.2015"));
        list.add(new Comment("admin", "comment2", 56, 32, "02.03.2015"));
        list.add(new Comment("user", "comment1", 456, 32, "30.03.2015"));
        list.add(new Comment("admin", "comment2", 56, 332, "02.03.2015"));

        list.add(new Comment("user", "comment1", 5126, 32, "02.03.1995"));
        list.add(new Comment("admin", "comment2", 456, 32, "02.10.2015"));
        list.add(new Comment("user", "comment1", 56, 323, "12.03.2015"));
        list.add(new Comment("admin", "comment2", 56, 312, "12.03.2015"));
        list.add(new Comment("user", "comment1", 536, 32, "17.03.2015"));
        list.add(new Comment("admin", "comment2", 56, 342, "17.03.2015"));

        //Collections.sort(list);
        spinner.setOnItemSelectedListener(this);






        adapter = new CommentAdapter(this, list);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);


    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        pos=position;
        Toast toast=Toast.makeText(this, position+"", Toast.LENGTH_SHORT);
        toast.show();
        Collections.sort(list);
        adapter = new CommentAdapter(this, list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
